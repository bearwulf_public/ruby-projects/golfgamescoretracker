# frozen_string_literal: true

# Cretes golf game model
class CreateGolfGames < ActiveRecord::Migration[7.1]
  def change
    create_table :golf_games do |t|
      t.date :date
      t.string :course_name
      t.integer :par
      t.integer :score

      t.timestamps
    end
  end
end
