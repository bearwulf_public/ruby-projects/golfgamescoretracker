# frozen_string_literal: true

require 'application_system_test_case'

class GolfGamesTest < ApplicationSystemTestCase
  setup do
    @golf_game = golf_games(:one)
  end

  test 'visiting the index' do
    visit golf_games_url
    assert_selector 'h1', text: 'Golf games'
  end

  test 'should create golf game' do
    visit golf_games_url
    click_on 'New golf game'

    fill_in 'Course name', with: @golf_game.course_name
    fill_in 'Date', with: @golf_game.date
    fill_in 'Par', with: @golf_game.par
    fill_in 'Score', with: @golf_game.score
    click_on 'Create Golf game'

    assert_text 'Golf game was successfully created'
    click_on 'Back'
  end

  test 'should update Golf game' do
    visit golf_game_url(@golf_game)
    click_on 'Edit this golf game', match: :first

    fill_in 'Course name', with: @golf_game.course_name
    fill_in 'Date', with: @golf_game.date
    fill_in 'Par', with: @golf_game.par
    fill_in 'Score', with: @golf_game.score
    click_on 'Update Golf game'

    assert_text 'Golf game was successfully updated'
    click_on 'Back'
  end

  test 'should destroy Golf game' do
    visit golf_game_url(@golf_game)
    click_on 'Destroy this golf game', match: :first

    assert_text 'Golf game was successfully destroyed'
  end
end
