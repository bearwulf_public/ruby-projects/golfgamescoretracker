# frozen_string_literal: true

require 'test_helper'

class GolfGamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @golf_game = golf_games(:one)
  end

  test 'should get index' do
    get golf_games_url
    assert_response :success
  end

  test 'should get new' do
    get new_golf_game_url
    assert_response :success
  end

  test 'should create golf_game' do
    assert_difference('GolfGame.count') do
      post golf_games_url,
           params: { golf_game: { course_name: @golf_game.course_name, date: @golf_game.date, par: @golf_game.par,
                                  score: @golf_game.score } }
    end

    assert_redirected_to golf_game_url(GolfGame.last)
  end

  test 'should show golf_game' do
    get golf_game_url(@golf_game)
    assert_response :success
  end

  test 'should get edit' do
    get edit_golf_game_url(@golf_game)
    assert_response :success
  end

  test 'should update golf_game' do
    patch golf_game_url(@golf_game),
          params: { golf_game: { course_name: @golf_game.course_name, date: @golf_game.date, par: @golf_game.par,
                                 score: @golf_game.score } }
    assert_redirected_to golf_game_url(@golf_game)
  end

  test 'should destroy golf_game' do
    assert_difference('GolfGame.count', -1) do
      delete golf_game_url(@golf_game)
    end

    assert_redirected_to golf_games_url
  end
end
