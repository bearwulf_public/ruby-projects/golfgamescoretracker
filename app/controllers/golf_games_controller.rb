# frozen_string_literal: true

class GolfGamesController < ApplicationController
  before_action :set_golf_game, only: %i[show edit update destroy]

  # GET /golf_games or /golf_games.json
  def index
    @golf_games = GolfGame.all
  end

  # GET /golf_games/1 or /golf_games/1.json
  def show; end

  # GET /golf_games/new
  def new
    @golf_game = GolfGame.new
  end

  # GET /golf_games/1/edit
  def edit; end

  # POST /golf_games or /golf_games.json
  def create
    @golf_game = GolfGame.new(golf_game_params)

    respond_to do |format|
      if @golf_game.save
        format.html { redirect_to golf_game_url(@golf_game), notice: 'Golf game was successfully created.' }
        format.json { render :show, status: :created, location: @golf_game }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @golf_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /golf_games/1 or /golf_games/1.json
  def update
    respond_to do |format|
      if @golf_game.update(golf_game_params)
        format.html { redirect_to golf_game_url(@golf_game), notice: 'Golf game was successfully updated.' }
        format.json { render :show, status: :ok, location: @golf_game }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @golf_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /golf_games/1 or /golf_games/1.json
  def destroy
    @golf_game.destroy!

    respond_to do |format|
      format.html { redirect_to golf_games_url, notice: 'Golf game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_golf_game
    @golf_game = GolfGame.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def golf_game_params
    params.require(:golf_game).permit(:date, :course_name, :par, :score)
  end
end
