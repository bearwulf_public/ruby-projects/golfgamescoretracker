# frozen_string_literal: true

json.array! @golf_games, partial: 'golf_games/golf_game', as: :golf_game
