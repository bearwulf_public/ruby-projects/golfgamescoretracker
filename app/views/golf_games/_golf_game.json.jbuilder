# frozen_string_literal: true

json.extract! golf_game, :id, :date, :course_name, :par, :score, :created_at, :updated_at
json.url golf_game_url(golf_game, format: :json)
