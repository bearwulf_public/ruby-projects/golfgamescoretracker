# frozen_string_literal: true

json.partial! 'golf_games/golf_game', golf_game: @golf_game
